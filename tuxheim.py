################################################
###
### Author: Der Linux Wikinger
###
### This tool is originally licenced by GNU GPLv3
### Image source:
### https://pixabay.com/
### Image Authors:
### OpenClipart-Vectors
### Clker-Free-Vector-Images
### David Colegado
### All images are licensed by the "Pixabay License" and are free to use for commercial and non-commercial purposes:
### https://pixabay.com/de/service/license/
###
### Version 1.1
###
################################################

### Module import
from tkinter import *
from tkinter import ttk
import os
import tkinter.messagebox
import paramiko
import time
import threading
from PIL import ImageTk, Image
import tkinter.scrolledtext as st
import tkinter as tk
import math
import socket
import platform

def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)

### Set variables
BG_COLOR = '#424242'

if "nt" in os.name or "windows" in os.name:
    font = "Arial"
elif "posix" in os.name or "linux" in os.name:
    font = "Ubuntu"
else:
    font = "Arial"

### -----------------------------------------------------------------------------
### Function section
### -----------------------------------------------------------------------------

### Make contextmenu function
def make_contextmenu(window):
	global context_menu
	context_menu = tkinter.Menu(window, tearoff=0)
	context_menu.add_command(label="Cut")
	context_menu.add_command(label="Copy")
	context_menu.add_command(label="Paste")
	context_menu.add_separator()
	context_menu.add_command(label="Select all")

### Show contextmenu function
def show_contextmenu(event):
	e_widget = event.widget
	context_menu.entryconfigure("Cut",command=lambda: e_widget.event_generate("<<Cut>>"))
	context_menu.entryconfigure("Copy",command=lambda: e_widget.event_generate("<<Copy>>"))
	context_menu.entryconfigure("Paste",command=lambda: e_widget.event_generate("<<Paste>>"))
	context_menu.entryconfigure("Select all",command=lambda: e_widget.select_range(0, 'end'))
	context_menu.tk.call("tk_popup", context_menu, event.x_root, event.y_root)

def treeview_sort_column(col, reverse, table):
    l = []
    for iid in table.get_children():
        value_iid = (table.set(iid, col), iid)
        l.append(value_iid)

    try:
        l.sort(reverse=reverse, key=lambda l: int(l[0]))
    except:
        l.sort(reverse=reverse)

    for index, (value, iid) in enumerate(l):
        table.move(iid, '', index)

    table.heading(col, command=lambda: treeview_sort_column(col, not reverse, table))


### Establish SSH connection
def connect_est():
    global hostname_ssh
    global display_name_hostname_ssh
    hostname_ssh = hostname.get()
    hostname_ssh = hostname_ssh.strip()
    display_name_hostname_ssh = hostname_ssh.upper()
    global username_ssh
    username_ssh = username.get()
    username_ssh = username_ssh.strip()
    global password_ssh
    password_ssh = password.get()
    port_ssh = port.get()

    if hostname_ssh == "":
        tkinter.messagebox.showerror('Tuxheim', 'No hostname or IP given')
    elif username_ssh == "":
        tkinter.messagebox.showerror('Tuxheim', 'No username given')
    elif password_ssh == "":
        tkinter.messagebox.showerror('Tuxheim', 'No password given')
    elif port_ssh == "":
        tkinter.messagebox.showerror('Tuxheim', 'No SSH port given')

    else:
        if "'" in password_ssh:
            password.delete(0, END)
            tkinter.messagebox.showerror('Tuxheim', 'Password contains invalid characters')
        else:
            try:
                ssh.connect(hostname=hostname_ssh, port=int(port_ssh), username=username_ssh, password=password_ssh)
            except Exception as e:
                password.delete(0, END)
                tkinter.messagebox.showerror('Tuxheim', e)

            else:
                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S which apt-get")
                exit_code_distro = stdout.channel.recv_exit_status()
                if exit_code_distro == 0:
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S getent group sudo | grep -iw " + str(username_ssh))
                    exit_code_getent = stdout.channel.recv_exit_status()
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S grep -i '" + str(username_ssh) + "' /etc/sudoers")
                    exit_code_sudoers = stdout.channel.recv_exit_status()
                    stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S grep -i "#' + str(username_ssh) + '" /etc/sudoers')
                    exit_code_sudoers_commented = stdout.channel.recv_exit_status()

                    if exit_code_getent != 0:
                        if exit_code_sudoers != 0:
                            ssh.close()
                            password.delete(0, END)
                            tkinter.messagebox.showerror('Tuxheim', 'Given user has not sufficient permissions')
                        elif exit_code_sudoers == 0:
                            if exit_code_sudoers_commented == 0:
                                ssh.close()
                                password.delete(0, END)
                                tkinter.messagebox.showerror('Tuxheim', 'Given user has not sufficient permissions')

                else:
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S getent group wheel | grep -iw " + str(username_ssh))
                    exit_code_getent = stdout.channel.recv_exit_status()
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S grep -i '" + str(username_ssh) + "' /etc/sudoers")
                    exit_code_sudoers = stdout.channel.recv_exit_status()
                    stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S grep -i "#' + str(username_ssh) + '" /etc/sudoers')
                    exit_code_sudoers_commented = stdout.channel.recv_exit_status()

                    if exit_code_getent != 0:
                        if exit_code_sudoers != 0:
                            if str(username_ssh).lower() != 'root':
                                ssh.close()
                                password.delete(0, END)
                                tkinter.messagebox.showerror('Tuxheim', 'Given user has not sufficient permissions')
                        elif exit_code_sudoers == 0:
                            if exit_code_sudoers_commented == 0:
                                ssh.close()
                                password.delete(0, END)
                                tkinter.messagebox.showerror('Tuxheim', 'Given user has not sufficient permissions')

                if ssh.get_transport() is not None:
                    check_connection()
                    tabs_enable()
                    connect_disable()
                    system_information()
                    list_admin()
                    list_groups()
                    hideBG()
                    distro_warn()
                    thread_connection_loop()
                    window.title("Tuxheim - " + display_name_hostname_ssh)
                    list_users_tree_function()
                    list_groups_tree_function()
                    logs_lbox_function()
                    fill_table_function()
                    get_automatic_updates_status()
                    get_firewall_status()
                    tkinter.messagebox.showinfo('Tuxheim', 'Successfully connected to:\n\n' + display_name_hostname_ssh)

### Close SSH connection
def connect_close():
    ssh.close()
    username_ssh = ""
    password_ssh = ""

### Grey out fields when SSH connection is open
def connect_disable():
    hostname.configure(state='disabled')
    username.configure(state='disabled')
    password.configure(state='disabled')
    port.configure(state='disabled')
    connect_btn.configure(state='disabled')
    disconnect_btn.configure(state='normal')
    terminal_btn.configure(state='normal')

### Set fields to normal when SSH connection is closed
def connect_enable():
    hostname.configure(state='normal')
    username.configure(state='normal')
    password.configure(state='normal')
    port.configure(state='normal')
    connect_btn.configure(state='normal')
    disconnect_btn.configure(state='disabled')
    terminal_btn.configure(state='disabled')
    install_packages_btn.configure(state='normal')
    remove_packages_btn.configure(state='normal')
    search_packages_btn.configure(state='normal')
    update_packages_btn.configure(state='normal')
    recreate_packages_cache_btn.configure(state='normal')
    check_packages_update_btn.configure(state='normal')
    search_packages_input.configure(state='normal')
    user.deselect()
    group.deselect()
    other.deselect()
    read.deselect()
    write.deselect()
    execute.deselect()
    user.deselect()
    recurse_permission.deselect()
    recurse.deselect()
    window.title("Tuxheim")

def tabs_enable():
    TAB_CONTROL.tab(GROUP_TAB, state='normal')
    TAB_CONTROL.tab(SYS_TAB, state='normal')
    TAB_CONTROL.tab(USER_GROUP_TAB, state='normal')
    TAB_CONTROL.tab(SUDO_TAB, state='normal')
    TAB_CONTROL.tab(DIR_TAB, state='normal')
    TAB_CONTROL.tab(SOFTWARE_TAB, state='normal')
    TAB_CONTROL.tab(LOGS_TAB, state='normal')

def tabs_disable():
    TAB_CONTROL.tab(SYS_TAB, state='disabled')
    TAB_CONTROL.tab(GROUP_TAB, state='disabled')
    TAB_CONTROL.tab(USER_GROUP_TAB, state='disabled')
    TAB_CONTROL.tab(SUDO_TAB, state='disabled')
    TAB_CONTROL.tab(DIR_TAB, state='disabled')
    TAB_CONTROL.tab(SOFTWARE_TAB, state='disabled')
    TAB_CONTROL.tab(LOGS_TAB, state='disabled')

def connection_loop():
    while True:
        time.sleep(5)
        try:
            if ssh.get_transport().is_active() == True:
                get_automatic_updates_status()
                get_firewall_status()
                pass
            elif ssh.get_transport().is_active() == False:
                connect_close()
                connect_enable()
                clear_users()
                clear_listbox_expiration()
                clear_groups()
                clear_add_rm_user()
                check_connection()
                connect_clear()
                tabs_disable()
                tkinter.messagebox.showerror('Tuxheim', 'Connection lost')
                return

        except:
            return

def thread_connection_loop():
    threading.Thread(target=connection_loop).start()

### Clear Entry field
def connect_clear():
    hostname.delete(0, END)
    input_expiration.delete(0, END)
    input_sudo.delete(0, END)
    ownership_user_input.delete(0, END)
    ownership_group_input.delete(0, END)
    path_input.delete(0, END)
    path_input.insert(0, "/")
    search_packages_input.delete(0, END)
    tree_software.delete(*tree_software.get_children())
    distro_warn_software_lbl.config(text="")
    reset_system_information()

    login = keep_login.get()
    if login == 0:
        password.delete(0, END)
        username.delete(0, END)
    tkinter.messagebox.showinfo('Tuxheim', 'Successfully disconnected from :\n\n' + display_name_hostname_ssh)

def list_groups():
    stdin, stdout, stderr = ssh.exec_command("getent group | cut -d: -f 1 | sort")
    list_groups = stdout.readlines()
    list_groups = [i.replace("\n", "") for i in list_groups]
    for i in list_groups:
        lbox_groups.insert("end", i)

def list_users():
    lbox_selection = lbox_groups.get(lbox_groups.curselection())
    stdin, stdout, stderr = ssh.exec_command("getent group " + str(lbox_selection) + " | cut -d: -f 4")
    list_users = stdout.readlines()
    list_users_joined = ",".join(list_users)
    list_users_list = list(list_users_joined.split(","))
    list_users_list = [i.replace("\n", "") for i in list_users_list]
    for i in list_users_list:
        lbox_users.insert("end", i)

def list_admin():
    lbox_sudo.delete(0, END)

    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S which apt-get")
    exit_code_soft_manager = stdout.channel.recv_exit_status()

    if exit_code_soft_manager == 0:
        stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S grep 'ALL*[[:space:]]*=*[[:space:]]*(*[[:space:]]*ALL*[[:space:]]*:*[[:space:]]*ALL*[[:space:]]*)*[[:space:]]**' /etc/sudoers | awk {'print $1'} | grep -v ^% | grep -v ^#")
        list_sudo_sudoers = stdout.readlines()
        stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S getent group sudo | awk -F':' '/sudo/{print $4}'")
        list_sudo_groups = stdout.readlines()
    else:
        stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S grep 'ALL=(ALL:ALL) ALL' /etc/sudoers | awk {'print $1'} | grep -v ^% | grep -v ^#")
        list_sudo_sudoers = stdout.readlines()
        stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S getent group wheel | awk -F':' '/wheel/{print $4}'")
        list_sudo_groups = stdout.readlines()

    ### List with all users from /etc/suoders
    list_sudo_sudoers = [i.replace("\n", "") for i in list_sudo_sudoers]

    ### Make everything lowercase
    for i in range(len(list_sudo_sudoers)):
        list_sudo_sudoers[i] = list_sudo_sudoers[i].lower()

    ### Delete duplicate entries
    list_sudo_sudoers = list(set(list_sudo_sudoers))

    ### Insert joined list into listbox
    for i in list_sudo_sudoers:
        lbox_sudo.insert("end", i)

def clear_groups():
    lbox_groups.delete(0, END)

def clear_users():
    lbox_users.delete(0, END)

def clear_listbox_expiration():
    lbox_expiration.delete(0, END)

def clear_add_rm_user():
    add_rm_user.delete(0, END)

def add_user_function():
    lbox_selection = lbox_groups.get(lbox_groups.curselection())
    add_user_ssh = add_rm_user.get()
    add_user_ssh = add_user_ssh.lower()

    stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S usermod -aG ' + str(lbox_selection) + ' ' + str(add_user_ssh))
    exit_code = stdout.channel.recv_exit_status()

    if lbox_selection == "":
        tkinter.messagebox.showerror('Tuxheim', 'Group not defined')

    elif exit_code == 0:
        tkinter.messagebox.showinfo('Tuxheim', 'User added')
        clear_users()
        list_users()

    elif exit_code == 6:
        tkinter.messagebox.showerror('Tuxheim', 'User not found')

    else:
        tkinter.messagebox.showerror('Tuxheim', 'No User specified')

def rm_user_funtion():
    lbox_selection = lbox_groups.get(lbox_groups.curselection())
    rm_user_ssh = lbox_users.get(lbox_users.curselection())

    if str(lbox_selection) == "sudo":
        stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S sed -i "/' + str(rm_user_ssh) + '/Id" /etc/sudoers')
        stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S gpasswd --delete ' + str(rm_user_ssh) + ' ' + str(lbox_selection))
        exit_code = stdout.channel.recv_exit_status()

        if exit_code == 0:
            tkinter.messagebox.showinfo('Tuxheim', 'User removed')
            clear_users()
            list_users()

        elif exit_code == 3:
            tkinter.messagebox.showerror('Tuxheim', 'User is not member of this group')

        else:
            tkinter.messagebox.showerror('Tuxheim', 'No User specified')

    else:
        stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S gpasswd --delete ' + str(rm_user_ssh) + ' ' + str(lbox_selection))
        exit_code = stdout.channel.recv_exit_status()

        if exit_code == 0:
            tkinter.messagebox.showinfo('Tuxheim', 'User removed')
            clear_users()
            list_users()

        elif exit_code == 3:
            tkinter.messagebox.showerror('Tuxheim', 'User is not member of this group')

        else:
            tkinter.messagebox.showerror('Tuxheim', 'No User specified')

def check_connection():
    if ssh.get_transport() is not None:
        ssh.get_transport().is_active()
        status_text.config(text="Connected", fg="green", font=("Arial", 10, 'bold'))
    else:
        status_text.config(text="Not connected", fg="red", font=("Arial", 10, 'bold'))
        hideBG()

def items_selected(event):
    lbox_users.delete(0, END)
    lbox_selection = lbox_groups.get(lbox_groups.curselection())
    stdin, stdout, stderr = ssh.exec_command("getent group " + str(lbox_selection) + " | cut -d: -f 4")
    list_users = stdout.readlines()
    list_users_joined = ",".join(list_users)
    list_users_list = list(list_users_joined.split(","))
    list_users_list = [i.replace("\n", "") for i in list_users_list]
    for i in list_users_list:
        lbox_users.insert("end", i)

def sudo_expiration_selected(event):
    hostname_sudo = hostname_short.lower().strip()
    lbox_expiration.delete(0, END)
    lbox_sudo_selection = lbox_sudo.get(lbox_sudo.curselection())

    ### Get current Unix-Time
    stdin, stdout, stderr = ssh.exec_command('date +%s')
    current_time_list = stdout.readlines()
    current_time_list = [int(x) for x in current_time_list]
    current_time = ''
    for element in current_time_list:
        current_time += str(element)
    current_time = int(current_time)

    ### Get expiration time
    stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S grep expiration_date /etc/cron.daily/sudo_expiration_' + str(hostname_sudo) + '_' + str(lbox_sudo_selection) + ' | head -n1 | cut -f2 -d\"=\"')
    expiration_time_list = stdout.readlines()

    try:
        expiration_time_list = [int(x) for x in expiration_time_list]
        expiration_time = ''
        for element in expiration_time_list:
            expiration_time += str(element)
        expiration_time = int(expiration_time)
        expiration_in_seconds = expiration_time - current_time  ### Time left in seconds
        expiration_in_minutes = expiration_in_seconds / 60  ### Time left in minutes
        expiration_in_hours = expiration_in_minutes / 60  ### Time left in hours
        expiration_in_days = expiration_in_hours / 24  ### Time left in days
        expiration_rounded = int(math.ceil(expiration_in_days))

        if expiration_rounded == 0:
            lbox_expiration.insert("end", '>1')
        else:
            lbox_expiration.insert("end", expiration_rounded)
    except:
        lbox_expiration.insert("end", "Unlimited")

### Create popup menu
def about_popup():
    tkinter.messagebox.showinfo('Tuxheim', 'Version:\n1.1\n\nAuthor of this software:\nDer Linux Wikinger\n\nQuestions or suggestions for improvement:\nroot@derlinuxwikinger.com'
                                         '\n\nYou may find this tool on gitlab:'
                                         '\nhttps://gitlab.com/derlinuxwikinger/tuxheim'
                                         '\nOriginally licenced by GNU GPLv3'
                                         '\n\nImage source:'
                                         '\nhttps://pixabay.com/'
                                         '\n\nImage Authors:'
                                         '\nOpenClipart-Vectors'
                                         '\nClker-Free-Vector-Images'
                                         '\n\nAll images are licensed by the "Pixabay License" and are free to use for commercial and non-commercial purposes:'
                                         '\nhttps://pixabay.com/de/service/license/')

def error_popup():
    if ssh.get_transport() is None:
        tkinter.messagebox.showerror('Tuxheim', 'Connection failed')

def sudo_add_function():
    hostname_sudo = hostname_short.lower().strip()
    add_sudo = input_sudo.get().lower().strip()
    expiration_sudo = input_expiration.get().strip()
    input_expiration_checkbutton = unlimited_sudo.get()

    stdin, stdout, stderr = ssh.exec_command('/usr/bin/sudo -S <<< "' + str(password_ssh) + '" true ; /usr/bin/sudo grep -qiw "' + str(add_sudo) + '" /etc/sudoers')
    exit_code = stdout.channel.recv_exit_status()
    stdin, stdout, stderr = ssh.exec_command('/usr/bin/sudo -S <<< "' + str(password_ssh) + '" true ; /usr/bin/sudo grep -qiw "#' + str(add_sudo) + '" /etc/sudoers')
    exit_code_commented = stdout.channel.recv_exit_status()
    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S date +%s")
    starting_date_list = stdout.readlines()
    starting_date_list = [int(x) for x in starting_date_list]
    starting_date = ''
    for element in starting_date_list:
        starting_date += str(element)
    starting_date = int(starting_date)

    try:
        expiration_sudo = int(expiration_sudo)
        expiration_sudo = expiration_sudo * 86400
        expiration_sudo = expiration_sudo + starting_date
    except:
        pass

    if add_sudo == "":
        tkinter.messagebox.showerror('Tuxheim', 'No user specified')

    elif add_sudo.lower() == "root":
        tkinter.messagebox.showerror('Tuxheim', 'root is not allowed to be added as admin')

    elif expiration_sudo == "" and input_expiration_checkbutton == 0:
        tkinter.messagebox.showerror('Tuxheim', 'No expiration value set')

    elif isinstance(expiration_sudo, int) == False and input_expiration_checkbutton == 0:
        tkinter.messagebox.showerror('Tuxheim', 'No valid expiration number')

    elif exit_code == 0 and exit_code_commented != 0:
        tkinter.messagebox.showinfo('Tuxheim', 'User already added as admin')
    else:
        stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S /usr/bin/sudo id ' + str(add_sudo))
        exit_code_user_avail = stdout.channel.recv_exit_status()

        if exit_code_user_avail != 0:
            MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'User couldn\'t be found. Add as admin anyway?', icon='warning')
            if MsgBox == 'yes':

                stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S bash -c "echo \'' + str(add_sudo) + ' ALL=(ALL:ALL) ALL\' >> /etc/sudoers"')
                exit_code_add_sudo = stdout.channel.recv_exit_status()

                if input_expiration_checkbutton == 0:
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"#!/bin/bash\" > /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                    time.sleep(1)
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"current_date=\$(date +%s)\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                    stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S bash -c "echo expiration_date=' + str(expiration_sudo) + ' >> /etc/cron.daily/sudo_expiration_' + str(hostname_sudo) + "_" + str(add_sudo) + '"')
                    time.sleep(1)
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"if [[ \$current_date > \$expiration_date ]]; then\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                    time.sleep(1)
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"  sed -i /" + str(add_sudo) + "/Id /etc/sudoers\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"  find /etc/cron.daily/ -name sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + " -exec rm {} \\;\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"fi\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")

                    stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S chmod +x /etc/cron.daily/sudo_expiration_' + str(hostname_sudo) + "_" + str(add_sudo))

                else:
                    if exit_code_add_sudo == 0:
                        tkinter.messagebox.showinfo('Tuxheim', 'User successfully added as admin')
                    else:
                        tkinter.messagebox.showerror('Tuxheim', 'Failed to add user as admin')

        else:
            stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S bash -c "echo \'' + str(add_sudo) + ' ALL=(ALL:ALL) ALL\' >> /etc/sudoers"')
            exit_code_add_sudo = stdout.channel.recv_exit_status()

            if input_expiration_checkbutton == 0:
                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"#!/bin/bash\" > /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                time.sleep(1)
                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"current_date=\$(date +%s)\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S bash -c "echo expiration_date=' + str(expiration_sudo) + ' >> /etc/cron.daily/sudo_expiration_' + str(hostname_sudo) + "_" + str(add_sudo) + '"')
                time.sleep(1)
                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"if [[ \$current_date > \$expiration_date ]]; then\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                time.sleep(1)
                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"  sed -i /" + str(add_sudo) + "/Id /etc/sudoers\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"  find /etc/cron.daily/ -name sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + " -exec rm {} \\;\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")
                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S bash -c 'echo \"fi\" >> /etc/cron.daily/sudo_expiration_" + str(hostname_sudo) + "_" + str(add_sudo) + "'")

                stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S chmod +x /etc/cron.daily/sudo_expiration_' + str(hostname_sudo) + "_" + str(add_sudo))

            if exit_code_add_sudo == 0:
                tkinter.messagebox.showinfo('Tuxheim', 'User successfully added as admin')
            else:
                tkinter.messagebox.showerror('Tuxheim', 'Failed to add user as admin')

def sudo_rm_function():
    hostname_sudo = hostname_short.lower().strip()
    rm_sudo = lbox_sudo.get(lbox_sudo.curselection())

    if rm_sudo == "":
        tkinter.messagebox.showerror('Tuxheim', 'No user specified')
    elif rm_sudo.lower() == "root":
        tkinter.messagebox.showerror('Tuxheim', 'You are not allowed to remove root as sudo')
    else:
        if exit_code_distro_warn == 0:
            stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S find /etc/cron.daily/ -name "sudo_expiration_' + str(hostname_sudo) + "_" + str(rm_sudo) + '" -exec rm {} \\;')
            stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S gpasswd --delete ' + str(rm_sudo) + ' sudo')
            stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S sed -i "/' + str(rm_sudo) + '/Id" /etc/sudoers')
            exit_code = stdout.channel.recv_exit_status()

            if exit_code == 0:
                tkinter.messagebox.showinfo('Tuxheim', 'User successfully removed')
            else:
                tkinter.messagebox.showerror('Tuxheim', 'User not found or it\'s not a member of sudo')
        else:
            stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S find /etc/cron.daily/ -name "sudo_expiration_' + str(hostname_sudo) + "_" + str(rm_sudo) + '" -exec rm {} \\;')
            stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S sed -i "/' + str(rm_sudo) + '/Id" /etc/sudoers')
            stdin, stdout, stderr = ssh.exec_command('echo "' + str(password_ssh) + '" | /usr/bin/sudo -S gpasswd --delete ' + str(rm_sudo) + ' wheel')
            exit_code = stdout.channel.recv_exit_status()
            if rm_sudo == "":
                tkinter.messagebox.showerror('Tuxheim', 'No user specified')
            elif exit_code == 0:
                tkinter.messagebox.showinfo('Tuxheim', 'User successfully removed')
            else:
                tkinter.messagebox.showerror('Tuxheim', 'User not found or it\'s not a member of sudo')

def click_sudo_checkbutton():
    input_expiration_checkbutton = unlimited_sudo.get()

    if input_expiration_checkbutton == 1:
        input_expiration.delete(0, END)
        input_expiration.config(state=DISABLED)
    else:
        input_expiration.config(state='normal')

def click_uid_checkbutton():
    input_uid_checkbutton = set_uid.get()

    if input_uid_checkbutton == 1:
        input_uid.delete(0, END)
        input_uid.config(state='normal')
    else:
        input_uid.config(state=DISABLED)

def click_gid_checkbutton():
    input_gid_checkbutton = set_gid.get()

    if input_gid_checkbutton == 1:
        input_gid.delete(0, END)
        input_gid.config(state='normal')
    else:
        input_gid.config(state=DISABLED)

def list_users_tree_function():
    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S awk -F ':' '{print $1 \" \" $3 \" \" $4}' /etc/passwd")
    list_users = stdout.readlines()
    list_users = [i.replace("\n", "") for i in list_users]

    for i in tree_user_group_user.get_children():
        tree_user_group_user.delete(i)
    for i in list_users:
        tree_user_group_user.insert('', 'end', values=(i))

def list_groups_tree_function():
    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S awk -F ':' '{print $1 \" \" $3}' /etc/group")
    list_groups = stdout.readlines()
    list_groups = [i.replace("\n", "") for i in list_groups]

    for i in tree_user_group_group.get_children():
        tree_user_group_group.delete(i)
    for i in list_groups:
        tree_user_group_group.insert('', 'end', values=(i))

def delete_user_function():
    del_home_func = del_home.get()

    try:
        selectedItem = tree_user_group_user.selection()[0]
    except:
        tkinter.messagebox.showinfo('Tuxheim', 'No user selected')
    else:
        selectedItem = tree_user_group_user.selection()[0]
        selectedItem = tree_user_group_user.item(selectedItem)['values'][0]

        if del_home_func == 0:
            MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Are you sure you want to delete the user ' + selectedItem + ' ?', icon='warning')
            if MsgBox == 'yes':
                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S userdel " + str(selectedItem))
                exit_code = stdout.channel.recv_exit_status()
                if exit_code == 0:
                    tkinter.messagebox.showinfo('Tuxheim', str(selectedItem) + ' successfully deleted')
                else:
                    tkinter.messagebox.showerror('Tuxheim', 'Failed to delete ' + str(selectedItem))
        elif del_home_func == 1:
            MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Are you sure you want to delete the user ' + selectedItem + ' with it\'s /home-directory ?', icon='warning')
            if MsgBox == 'yes':
                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S userdel -r " + str(selectedItem))
                exit_code = stdout.channel.recv_exit_status()
                if exit_code == 0:
                    tkinter.messagebox.showinfo('Tuxheim', str(selectedItem) + ' successfully deleted')
                else:
                    tkinter.messagebox.showerror('Tuxheim', 'Failed to delete ' + str(selectedItem))

def delete_group_function():
    try:
        selectedItem = tree_user_group_group.selection()[0]
    except:
        tkinter.messagebox.showinfo('Tuxheim', 'No group selected')
    else:
        selectedItem = tree_user_group_group.selection()[0]
        selectedItem = tree_user_group_group.item(selectedItem)['values'][0]
        MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Are you sure you want to delete the group ' + selectedItem + ' ?', icon='warning')
        if MsgBox == 'yes':
            stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S groupdel " + str(selectedItem))
            exit_code = stdout.channel.recv_exit_status()
            error_msg = stderr.readlines()
            error_msg = str(error_msg)
            error_msg = error_msg.replace("['[sudo] password for ", "")
            error_msg = error_msg.replace("\\n']", "")
            if exit_code == 0:
                tkinter.messagebox.showinfo('Tuxheim', str(selectedItem) + ' successfully deleted')
            else:
                tkinter.messagebox.showerror('Tuxheim', 'Failed to delete ' + str(selectedItem) + str(error_msg))

def create_user_function():
    uid_checkbutton = set_uid.get()
    uid_input = input_uid.get()
    create_user_func = create_user_input.get()
    create_home_func = create_home.get()

    try:
        uid_input = int(uid_input)
    except:
        pass

    if create_user_func == "":
        tkinter.messagebox.showerror('Tuxheim', 'No username given')
    else:
        if uid_input == '' and uid_checkbutton == 1:
            tkinter.messagebox.showerror('Tuxheim', 'No UID given')
        elif isinstance(uid_input, int) == False and uid_checkbutton == 1:
            tkinter.messagebox.showerror('Tuxheim', 'No valid UID given')

        elif create_home_func == 0:
            MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Do you want to create the user ' + str(create_user_func) + ' without /home-directory ?', icon='warning')
            if MsgBox == 'yes':
                if uid_checkbutton == 1:
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S useradd -u " + str(uid_input) + " " + str(create_user_func))
                    exit_code = stdout.channel.recv_exit_status()
                    error_msg = stderr.readlines()
                    error_msg = str(error_msg)
                    error_msg = error_msg.replace("['[sudo] password for", "")
                    error_msg = error_msg.replace("\\n']", "")
                    if exit_code == 0:
                        tkinter.messagebox.showinfo('Tuxheim', 'User successfully created')
                    else:
                        tkinter.messagebox.showerror('Tuxheim', 'Failed to create user\n' + str(error_msg))
                else:
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S useradd " + str(create_user_func))
                    exit_code = stdout.channel.recv_exit_status()
                    error_msg = stderr.readlines()
                    error_msg = str(error_msg)
                    error_msg = error_msg.replace("['[sudo] password for", "")
                    error_msg = error_msg.replace("\\n']", "")
                    if exit_code == 0:
                        tkinter.messagebox.showinfo('Tuxheim', 'User successfully created')
                    else:
                        tkinter.messagebox.showerror('Tuxheim', 'Failed to create user\n' + str(error_msg))

        elif create_home_func == 1:
            MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Do you want to create the user ' + str(create_user_func) + ' with /home-directory ?', icon='warning')
            if MsgBox == 'yes':
                if uid_checkbutton == 1:
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S useradd -m -u " + str(uid_input) + " " + str(create_user_func))
                    exit_code = stdout.channel.recv_exit_status()
                    error_msg = stderr.readlines()
                    error_msg = str(error_msg)
                    error_msg = error_msg.replace("['[sudo] password for ", "")
                    error_msg = error_msg.replace("\\n']", "")
                    if exit_code == 0:
                        tkinter.messagebox.showinfo('Tuxheim', 'User successfully created')
                    else:
                        tkinter.messagebox.showerror('Tuxheim', 'Failed to create user\n' + str(error_msg))
                else:
                    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S useradd -m " + str(create_user_func))
                    exit_code = stdout.channel.recv_exit_status()
                    error_msg = stderr.readlines()
                    error_msg = str(error_msg)
                    error_msg = error_msg.replace("['[sudo] password for ", "")
                    error_msg = error_msg.replace("\\n']", "")
                    if exit_code == 0:
                        tkinter.messagebox.showinfo('Tuxheim', 'User successfully created')
                    else:
                        tkinter.messagebox.showerror('Tuxheim', 'Failed to create user\n' + str(error_msg))

def create_group_function():
    gid_checkbutton = set_gid.get()
    gid_input = input_gid.get()
    create_group_func = create_group_input.get()

    try:
        gid_input = int(gid_input)
    except:
        pass

    if create_group_func == "":
        tkinter.messagebox.showerror('Tuxheim', 'No group name given')
    elif gid_input == '' and gid_checkbutton == 1:
        tkinter.messagebox.showerror('Tuxheim', 'No GID given')
    elif isinstance(gid_input, int) == False and gid_checkbutton == 1:
        tkinter.messagebox.showerror('Tuxheim', 'No valid GID given')
    else:
        MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Do you want to create the group ' + str(create_group_func) + ' ?', icon='warning')
        if MsgBox == 'yes':
            if gid_checkbutton == 1:
                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S groupadd -g " + str(gid_input) + " " + str(create_group_func))
                exit_code = stdout.channel.recv_exit_status()
                error_msg = stderr.readlines()
                error_msg = str(error_msg)
                error_msg = error_msg.replace("['[sudo] password for ", "")
                error_msg = error_msg.replace("\\n']", "")
                if exit_code == 0:
                    tkinter.messagebox.showinfo('Tuxheim', 'Group successfully created')
                else:
                    tkinter.messagebox.showerror('Tuxheim', 'Failed to create group\n' + str(error_msg))
            else:
                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S groupadd " + str(create_group_func))
                exit_code = stdout.channel.recv_exit_status()
                error_msg = stderr.readlines()
                error_msg = str(error_msg)
                error_msg = error_msg.replace("['[sudo] password for ", "")
                error_msg = error_msg.replace("\\n']", "")
                if exit_code == 0:
                    tkinter.messagebox.showinfo('Tuxheim', 'Group successfully created')
                else:
                    tkinter.messagebox.showerror('Tuxheim', 'Failed to create group\n' + str(error_msg))

def create_user_enter(event):
    create_user_function()

def create_group_enter(event):
    create_group_function()

def fill_table_function():
    path = path_input.get()

    if path == "":
        tkinter.messagebox.showerror('Tuxheim', 'Please specify a path')
    else:
        stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S ls -laF " + str(path) + " | awk '{print $1, $3, $4, $NF}' | tail -n +4 | grep -v ^l")
        path = stdout.readlines()
        path = [i.replace("\n", "") for i in path]
        for i in tree_file_directory.get_children():
            tree_file_directory.delete(i)
        for i in path:
            tree_file_directory.insert('', 'end', values=(i))

def set_ownership_function():
    user = ownership_user_input.get()
    group = ownership_group_input.get()
    group = group.replace(" ", "\ ")
    recurse = recurse_owner.get()
    path = path_input.get()

    MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Are you sure you want to change the ownership of ' + path + ' ?', icon='warning')
    if MsgBox == 'yes':
        if path == "":
            tkinter.messagebox.showerror('Tuxheim', 'No path specified')
        elif user == "" and group == "":
            tkinter.messagebox.showerror('Tuxheim', 'At least user or group has to be specified')
        elif recurse == 1:
            if group == "":

                progressbar().start_progressbar()

                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S chown -R " + str(user) + " " + str(path))
                exit_code = stdout.channel.recv_exit_status()
                if exit_code == 0:
                    close_progressbar()
                    tkinter.messagebox.showinfo('Tuxheim', 'Ownership successfully changed')
                else:
                    close_progressbar()
                    tkinter.messagebox.showerror('Tuxheim', 'Ownership failed to change')

            else:

                progressbar().start_progressbar()

                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S chown -R " + str(user) + ":" + str(group) + " " + str(path))
                exit_code = stdout.channel.recv_exit_status()
                if exit_code == 0:
                    close_progressbar()
                    tkinter.messagebox.showinfo('Tuxheim', 'Ownership successfully changed')
                else:
                    close_progressbar()
                    tkinter.messagebox.showerror('Tuxheim', 'Ownership failed to change')

        elif recurse == 0:
            if group == "":

                progressbar().start_progressbar()

                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S chown " + str(user) + " " + str(path))
                exit_code = stdout.channel.recv_exit_status()
                if exit_code == 0:
                    close_progressbar()
                    tkinter.messagebox.showinfo('Tuxheim', 'Ownership successfully changed')
                else:
                    close_progressbar()
                    tkinter.messagebox.showerror('Tuxheim', 'Ownership failed to change')

            else:

                progressbar().start_progressbar()

                stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S chown " + str(user) + ":" + str(group) + " " + str(path))
                exit_code = stdout.channel.recv_exit_status()
                if exit_code == 0:
                    close_progressbar()
                    tkinter.messagebox.showinfo('Tuxheim', 'Ownership successfully changed')
                else:
                    close_progressbar()
                    tkinter.messagebox.showerror('Tuxheim', 'Ownership failed to change')

def thread_set_ownership_function():
    threading.Thread(target=set_ownership_function).start()

def add_permissions_function():
    path = path_input.get()
    recurse = recurse_permissions.get()
    read = read_permissions.get()
    write = write_permissions.get()
    execute = execute_permissions.get()
    user = user_permissions.get()
    group = group_permissions.get()
    other = other_permissions.get()
    if read == 1:
        read = "r"
    else:
        read = ""

    if write == 1:
        write = "w"
    else:
        write = ""

    if execute == 1:
        execute = "x"
    else:
        execute = ""

    if user == 1:
        user = "u"
    else:
        user = ""

    if group == 1:
        group = "g"
    else:
        group = ""

    if other == 1:
        other = "o"
    else:
        other = ""

    rwx = read+write+execute
    ugo = user+group+other

    MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Are you sure you want to change the permissions of ' + path + ' ?', icon='warning')
    if MsgBox == 'yes':
        if path == "":
            tkinter.messagebox.showerror('Tuxheim', 'No path specified')
        elif read == 0 and write == 0 and execute == 0:
            tkinter.messagebox.showerror('Tuxheim', 'At least one option has to be specified')
        elif recurse == 1:

            progressbar().start_progressbar()

            stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S chmod -R " + str(ugo) + "+" + str(rwx) + " " + str(path))
            exit_code = stdout.channel.recv_exit_status()
            if exit_code == 0:
                close_progressbar()
                tkinter.messagebox.showinfo('Tuxheim', 'Permissions successfully changed')
            else:
                close_progressbar()
                tkinter.messagebox.showerror('Tuxheim', 'Permissions failed to change')
        elif recurse == 0:

            progressbar().start_progressbar()

            stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S chmod " + str(ugo) + "+" + str(rwx) + " " + str(path))
            exit_code = stdout.channel.recv_exit_status()
            if exit_code == 0:
                close_progressbar()
                tkinter.messagebox.showinfo('Tuxheim', 'Permissions successfully changed')
            else:
                close_progressbar()
                tkinter.messagebox.showerror('Tuxheim', 'Permissions failed to change')

def thread_add_permissions_function():
    threading.Thread(target=add_permissions_function).start()

def del_permissions_function():
    path = path_input.get()
    recurse = recurse_permissions.get()
    read = read_permissions.get()
    write = write_permissions.get()
    execute = execute_permissions.get()
    user = user_permissions.get()
    group = group_permissions.get()
    other = other_permissions.get()

    if read == 1:
        read = "r"
    else:
        read = ""

    if write == 1:
        write = "w"
    else:
        write = ""

    if execute == 1:
        execute = "x"
    else:
        execute = ""

    if user == 1:
        user = "u"
    else:
        user = ""

    if group == 1:
        group = "g"
    else:
        group = ""

    if other == 1:
        other = "o"
    else:
        other = ""

    rwx = read+write+execute
    ugo = user+group+other

    MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Are you sure you want to change the permissions of ' + path + ' ?', icon='warning')
    if MsgBox == 'yes':
        if path == "":
            tkinter.messagebox.showerror('Tuxheim', 'No path specified')
        elif read == 0 and write == 0 and execute == 0:
            tkinter.messagebox.showerror('Tuxheim', 'At least one option has to be specified')
        elif recurse == 1:

            progressbar().start_progressbar()

            stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S chmod -R " + str(ugo) + "-" + str(rwx) + " " + str(path))
            exit_code = stdout.channel.recv_exit_status()
            if exit_code == 0:
                close_progressbar()
                tkinter.messagebox.showinfo('Tuxheim', 'Permissions successfully changed')
            else:
                close_progressbar()
                tkinter.messagebox.showerror('Tuxheim', 'Permissions failed to change')
        elif recurse == 0:

            progressbar().start_progressbar()

            stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S chmod " + str(ugo) + "-" + str(rwx) + " " + str(path))
            exit_code = stdout.channel.recv_exit_status()
            if exit_code == 0:
                close_progressbar()
                tkinter.messagebox.showinfo('Tuxheim', 'Permissions successfully changed')
            else:
                close_progressbar()
                tkinter.messagebox.showerror('Tuxheim', 'Permissions failed to change')

def thread_del_permissions_function():
    threading.Thread(target=del_permissions_function).start()

def logs_lbox_function():
    lbox_logs.delete(0, END)
    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S find /var/log/ -type f -name '*.log' && find /var/log/ -type f -name 'syslog' && find /var/log/ -type f -name 'messages' | xargs file | grep ASCII")
    logs_path = stdout.readlines()
    logs_path = [i.replace("\n", "") for i in logs_path]

    for i in logs_path:
        lbox_logs.insert("end", i)

def open_terminal_function():
    hostname_ssh = hostname.get()
    username_ssh = username.get()
    if "nt" in os.name or "windows" in os.name:
        os.system('start /B start /min c:\Windows\system32\cmd.exe @cmd /k c:/"Program Files (x86)"/PuTTY/putty.exe -ssh ' + str(username_ssh) + '@' + str(hostname_ssh) + ' -pw "' + str(password_ssh) + '"')
    elif "posix" in os.name or "linux" in os.name:
        stdin, stdout, stderr = ssh.exec_command('apt list --installed | grep -i gnome-terminal')
        exit_code_gnome_terminal = stdout.channel.recv_exit_status()

        if exit_code_gnome_terminal == 0:
            os.system("gnome-terminal -- bash -c 'ssh " + str(username_ssh) + "@" + str(hostname_ssh) + "'")
        elif exit_code_gnome_terminal != 0:
            tkinter.messagebox.showinfo('Tuxheim', 'Please install gnome-terminal to use this function')

def search_packages_function():
    search_packages = search_packages_input.get()
    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S which apt-get")
    exit_code_soft_manager = stdout.channel.recv_exit_status()

    if search_packages == "":
        tkinter.messagebox.showinfo('Tuxheim', 'No package given')
    else:
        if exit_code_soft_manager == 0:
            stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S apt list | awk \'{print $1, $2, $4}\' | awk '{sub(/\/.*$/,\"\",$1); print $1\" \"$2\" \"$3}\' | sed \'s/\[/ /g\' | sed \'s/\]/ /g\' | sed s/,// | awk -F\",\" \'{print $1}\' | grep -i " + str(search_packages))
            software_list = stdout.readlines()
            software_list = [i.replace("\n", "") for i in software_list]
            for i in tree_software.get_children():
                tree_software.delete(i)
            for i in software_list:
                tree_software.insert('', 'end', values=(i))

def search_packages_function_enter(event):
    search_packages_function()

def install_package_function():
    try:
        selectedItem = tree_software.selection()[0]
    except:
        tkinter.messagebox.showinfo('Tuxheim', 'No package selected')
    else:
        selectedItem = tree_software.selection()[0]
        selectedItem = tree_software.item(selectedItem)['values'][0]

        MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Are you sure you want to install\n\n' + str(selectedItem), icon='warning')
        if MsgBox == 'yes':
            progressbar().start_progressbar()
            stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S apt -y install  " + str(selectedItem))
            exit_code = stdout.channel.recv_exit_status()
            if exit_code == 0:
                close_progressbar()
                search_packages_function()
                tkinter.messagebox.showinfo('Tuxheim', 'Successfully installed\n\n' + str(selectedItem))
            else:
                close_progressbar()
                search_packages_function()
                tkinter.messagebox.showerror('Tuxheim', 'Failed to install\n\n' + str(selectedItem))

def thread_install_package_function():
    threading.Thread(target=install_package_function).start()

def remove_package_function():
    try:
        selectedItem = tree_software.selection()[0]
    except:
        tkinter.messagebox.showinfo('Tuxheim', 'No package selected')
    else:
        selectedItem = tree_software.selection()[0]
        selectedItem = tree_software.item(selectedItem)['values'][0]

        MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Are you sure you want to remove\n\n' + str(selectedItem), icon='warning')
        if MsgBox == 'yes':
            progressbar().start_progressbar()
            stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S apt-get -y remove " + str(selectedItem))
            exit_code = stdout.channel.recv_exit_status()
            if exit_code == 0:
                close_progressbar()
                search_packages_function()
                tkinter.messagebox.showinfo('Tuxheim', 'Successfully removed\n\n' + str(selectedItem))
            else:
                close_progressbar()
                search_packages_function()
                tkinter.messagebox.showerror('Tuxheim', 'Failed to remove\n\n' + str(selectedItem))

def thread_remove_package_function():
    threading.Thread(target=remove_package_function).start()

def update_packages():
    MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Are you sure you want to update all packages?', icon='warning')
    if MsgBox == 'yes':
        progressbar().start_progressbar()
        stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S apt-get -y dist-upgrade")
        exit_code = stdout.channel.recv_exit_status()
        error_msg = stderr.readlines()
        error_msg = str(error_msg)
        if exit_code == 0:
            close_progressbar()
            tkinter.messagebox.showinfo('Tuxheim', 'Successfully updated packages')
        else:
            close_progressbar()
            tkinter.messagebox.showerror('Tuxheim', 'Failed to update packages\n\n' + str(error_msg))

def thread_update_packages():
    threading.Thread(target=update_packages).start()

def check_package_updates():
    global check_package_updates_window
    try:
        check_package_updates_window.destroy()
    except:
        pass

    check_package_updates_window = tk.Tk()
    check_package_updates_window.title("Available package updates")

    text_area = st.ScrolledText(check_package_updates_window, width=100, height=30, bg='black', fg='#00FF41')
    text_area.pack(fill=BOTH, expand=True, pady=10, padx=10)
    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S apt list --upgradable | tail -n +2")
    updates = stdout.read()
    text_area.insert(END, updates)
    text_area.configure(state='disabled')

def recreate_package_cache():
    progressbar().start_progressbar()
    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S apt update")
    exit_code = stdout.channel.recv_exit_status()
    error_msg = stderr.readlines()
    error_msg = str(error_msg)
    if exit_code == 0:
        close_progressbar()
        tkinter.messagebox.showinfo('Tuxheim', 'Successfully recreated package cache')
    else:
        close_progressbar()
        tkinter.messagebox.showerror('Tuxheim', 'Failed to recreate package cache\n\n' + str(error_msg))

def thread_recreate_package_cache():
    threading.Thread(target=recreate_package_cache).start()

### Distro warning
def distro_warn():
    global exit_code_distro_warn
    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S which apt-get")
    exit_code_distro_warn = stdout.channel.recv_exit_status()
    if exit_code_distro_warn != 0:
        ### Software management Tab
        install_packages_btn.configure(state='disabled')
        remove_packages_btn.configure(state='disabled')
        search_packages_btn.configure(state='disabled')
        update_packages_btn.configure(state='disabled')
        recreate_packages_cache_btn.configure(state='disabled')
        check_packages_update_btn.configure(state='disabled')
        search_packages_input.configure(state='disabled')
        distro_warn_software_lbl.config(text="Warning: Incompatible Distribution detected.\nSoftware management won't work!", fg="red", font=("Arial", 10, 'bold'))

def connection_enter(event):
    connect_est()

def admin_enter(event):
    sudo_add_function()
    list_admin()

def group_enter(event):
    add_user_function()

def list_path_enter(event):
    fill_table_function

def set_ownership_enter(event):
    thread_set_ownership_function()
    fill_table_function()

def system_information():
    global hostname_short
    ### Get short Hostname
    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S hostname")
    hostname_str_short = stdout.readlines()
    hostname_short = str(hostname_str_short)
    hostname_short = hostname_short.replace("['", "")
    hostname_short = hostname_short.replace("\\n']", "")
    hostname_short = hostname_short.split('.', 1)[0]

    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S hostname -f")
    hostname = stdout.readlines()
    hostname_str = str(hostname)
    hostname_str = hostname_str.replace("['", "")
    hostname_str = hostname_str.replace("\\n']", "")
    global hostname_full
    hostname_full = hostname_str
    hostname_lbl.config(text=hostname_str, font=("Arial", 10, 'bold'))

    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S awk -F= '/PRETTY/ {print $2}' /etc/os-release")
    os_release = stdout.readlines()
    os_release_str = str(os_release)
    os_release_str = os_release_str.replace("['\"", "")
    os_release_str = os_release_str.replace("\"\\n']", "")
    os_lbl.config(text=os_release_str, font=("Arial", 10, 'bold'))

    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S cat /var/run/reboot-required")
    exit_code_restart = stdout.channel.recv_exit_status()
    restart = stdout.readlines()

    if exit_code_restart == 0 and "required" in restart:
        restart_str = str(restart)
        restart_str = restart_str.replace("['*** ", "")
        restart_str = restart_str.replace(" ***\\n']", "")
        restart_lbl.config(text=restart_str, fg="red", font=("Arial", 10, 'bold'))
    else:
        restart_str = str(restart)
        restart_str = restart_str.replace("['*** ", "")
        restart_str = restart_str.replace(" ***\\n']", "")
        restart_lbl.config(text="No reboot required", fg="green", font=("Arial", 10, 'bold'))

    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S dmidecode | grep -A3 '^System Information' | grep -i product | sed 's/^[^:]*://'")
    model = stdout.readlines()
    model_str = str(model)
    model_str = model_str.replace("[' ", "")
    model_str = model_str.replace("\\n']", "")
    model_lbl.config(text=model_str, font=("Arial", 10, 'bold'))

def reset_system_information():
    hostname_lbl.config(text="UNKOWN", font=("Arial", 10, 'bold'))
    os_lbl.config(text="UNKOWN", font=("Arial", 10, 'bold'))
    restart_lbl.config(text="UNKOWN", font=("Arial", 10, 'bold'))
    model_lbl.config(text="UNKOWN", font=("Arial", 10, 'bold'))

def get_automatic_updates_status():
    if exit_code_distro_warn == 1:
        automatic_updates_lbl.config(text="Not available")
        firewall_status_lbl.config(text="Not available")
        automatic_updates_btn.configure(state='disabled')
    else:
        stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S service unattended-upgrades status")
        exit_code_automatic_updates_status = stdout.channel.recv_exit_status()
        if exit_code_automatic_updates_status == 0:
            automatic_updates_lbl.config(text="Activated", fg="green", font=("Arial", 10, 'bold'))
            automatic_updates_btn.configure(state='disabled')
        else:
            automatic_updates_lbl.config(text="Deactivated", fg="red", font=("Arial", 10, 'bold'))
            automatic_updates_btn.configure(state='active')

def get_firewall_status():
    if exit_code_distro_warn == 1:
        automatic_updates_lbl.config(text="Not available")
        firewall_status_lbl.config(text="Not available")
        activate_firewall_btn.configure(state='disabled')
    else:
        stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S ufw status | head -1 | awk '{print $2}'")
        ufw_status = stdout.readlines()
        ufw_status = ''.join(ufw_status)
        ufw_status = ufw_status.strip()

        if ufw_status == 'active':
            firewall_status_lbl.config(text="Activated", fg="green", font=("Arial", 10, 'bold'))
            activate_firewall_btn.configure(state='disabled')
        elif ufw_status == 'inactive':
            firewall_status_lbl.config(text="Deactivated", fg="red", font=("Arial", 10, 'bold'))
            activate_firewall_btn.configure(state='active')
        else:
            firewall_status_lbl.config(text="Firewall not installed or found", fg="red", font=("Arial", 10, 'bold'))
            activate_firewall_btn.configure(state='disabled')

def shutdown():
    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | sudo -S hostname -f")
    hostname = stdout.readlines()
    hostname_str = str(hostname)
    hostname_str = hostname_str.replace("['", "")
    hostname_str = hostname_str.replace("\\n']", "")

    MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Are you sure you want to shutdown ' + hostname_str + ' ?', icon='warning')
    if MsgBox == 'yes':
        stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S shutdown -h now")

def reboot():
    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | sudo -S hostname -f")
    hostname = stdout.readlines()
    hostname_str = str(hostname)
    hostname_str = hostname_str.replace("['", "")
    hostname_str = hostname_str.replace("\\n']", "")

    MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Are you sure you want to reboot ' + hostname_str + ' ?', icon='warning')
    if MsgBox == 'yes':
        stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S reboot")

def activate_automatic_updates():
    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S service unattended-upgrades start")
    exit_code_activate_automatic_updates = stdout.channel.recv_exit_status()
    progressbar().start_progressbar()
    if exit_code_activate_automatic_updates == 0:
        close_progressbar()
        tkinter.messagebox.showinfo('Tuxheim', 'Automatic updates successfully activated')
        automatic_updates_btn.configure(state='disabled')
    else:
        close_progressbar()
        tkinter.messagebox.showerror('Tuxheim', 'Failed to activate automatic updates')

def activate_firewall():
    MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Do you want to activate the firewall?', icon='warning')
    if MsgBox == 'yes':
        stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S ufw allow ssh")
        MsgBox = tkinter.messagebox.askquestion('Tuxheim', 'Do you want to keep port 22 (SSH) and 3389 (RDP) open?', icon='warning')
        if MsgBox == 'yes':
            stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S ufw allow ssh")
            stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S ufw allow 3389")

        stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S ufw --force enable")
        exit_code_activate_firewall = stdout.channel.recv_exit_status()
        progressbar().start_progressbar()
        if exit_code_activate_firewall == 0:
            close_progressbar()
            tkinter.messagebox.showinfo('Tuxheim', 'Firewall successfully activated')
            activate_firewall_btn.configure(state='disabled')
        else:
            close_progressbar()
            tkinter.messagebox.showerror('Tuxheim', 'Failed to activate firewall')

def show_logs():
    global logs_window
    try:
        logs_window.destroy()
    except:
        pass

    lbox_logs_selection = lbox_logs.get(lbox_logs.curselection())

    logs_window = tk.Tk()
    logs_window.title(lbox_logs_selection)

    text_area = st.ScrolledText(logs_window, width=180, height=45, bg='black', fg='#00FF41')
    text_area.pack(fill=BOTH, expand=True, pady=10, padx=10)
    stdin, stdout, stderr = ssh.exec_command("echo '" + str(password_ssh) + "' | /usr/bin/sudo -S cat " + lbox_logs_selection)
    logs = stdout.read()
    text_area.insert(END, logs)
    text_area.configure(state='disabled')

class progressbar():
    def start_progressbar(self):
        count = 0
        anim = None

        ### Create Loading Window
        loadingWindow = Toplevel(window)
        screen_width = window.winfo_screenwidth()
        screen_height = window.winfo_screenheight()
        ### Set center point
        center_x = int(screen_width / 1.5 - window_width / 1.3)
        center_y = int(screen_height / 1.5 - window_height / 2)
        loadingWindow.geometry(f'{600}x{250}+{center_x}+{center_y}')
        loadingWindow.overrideredirect(True)
        loadingWindow.config(bg='#424242')
        loadingWindow.lift()
        #loadingWindow.attributes("-topmost", True)
        loadingWindow_frame = Frame(loadingWindow, highlightbackground="black", highlightthickness=2, bg='#424242')
        loadingWindow_frame.pack()
        ### Create Progressbar
        loadingWindow_lbl = Label(loadingWindow, text="Tuxheim at work.\nPlease wait ...", font=("Arial", 25), bg='#424242', fg="white")
        loadingWindow_lbl.pack()
        global progressbar_btn
        progressbar_btn = Button(window, fg="green", command=lambda: [loadingWindow.destroy()], bg='white')

        def animation(count):
            global anim
            im2 = im[count]

            gif_label.configure(image=im2)
            count += 1
            if count == frames:
                count = 0
            anim = loadingWindow.after(60, lambda: animation(count))

        def stop_animation(loadingWindow):
            loadingWindow.after_cancel(anim)

        ### Disable buttons and entry fields
        shutdown_btn.configure(state='disabled')
        reboot_btn.configure(state='disabled')
        cur_admin_btn.configure(state='disabled')
        add_admin_user_btn.configure(state='disabled')
        rm_admin_user_btn.configure(state='disabled')
        input_sudo.configure(state='disabled')
        add_group_user_btn.configure(state='disabled')
        rm_group_user_btn.configure(state='disabled')
        add_rm_user.configure(state='disabled')
        list_path_btn.configure(state='disabled')
        path_input.configure(state='disabled')
        set_permissions_btn.configure(state='disabled')
        del_permissions_btn.configure(state='disabled')
        ownership_user_input.configure(state='disabled')
        ownership_group_input.configure(state='disabled')
        set_ownership_btn.configure(state='disabled')
        user.configure(state='disabled')
        group.configure(state='disabled')
        other.configure(state='disabled')
        read.configure(state='disabled')
        write.configure(state='disabled')
        execute.configure(state='disabled')
        recurse_permission.configure(state='disabled')
        recurse.configure(state='disabled')
        install_packages_btn.configure(state='disabled')
        remove_packages_btn.configure(state='disabled')
        search_packages_btn.configure(state='disabled')
        search_packages_input.configure(state='disabled')

        ### Start progressbar
        info = Image.open(resource_path(r"tuxheim_gif.gif"))
        frames = info.n_frames  # gives total number of frames that gif contains
        ### creating list of PhotoImage objects for each frames
        im = [tkinter.PhotoImage(file=resource_path('tuxheim_gif.gif'), format=f"gif -index {i}") for i in range(frames)]

        gif_label = tkinter.Label(loadingWindow, image="", bg=BG_COLOR)
        gif_label.pack(anchor="s", side="left")
        animation(count)

def close_progressbar():
    ### Close progressbar
    progressbar_btn.invoke()

    ### Enable buttons and entry field
    shutdown_btn.configure(state='normal')
    reboot_btn.configure(state='normal')
    cur_admin_btn.configure(state='normal')
    add_admin_user_btn.configure(state='normal')
    rm_admin_user_btn.configure(state='normal')
    input_sudo.configure(state='normal')
    add_group_user_btn.configure(state='normal')
    rm_group_user_btn.configure(state='normal')
    add_rm_user.configure(state='normal')
    list_path_btn.configure(state='normal')
    path_input.configure(state='normal')
    set_permissions_btn.configure(state='normal')
    del_permissions_btn.configure(state='normal')
    ownership_user_input.configure(state='normal')
    ownership_group_input.configure(state='normal')
    set_ownership_btn.configure(state='normal')
    user.configure(state='normal')
    group.configure(state='normal')
    other.configure(state='normal')
    read.configure(state='normal')
    write.configure(state='normal')
    execute.configure(state='normal')
    recurse_permission.configure(state='normal')
    recurse.configure(state='normal')
    install_packages_btn.configure(state='normal')
    remove_packages_btn.configure(state='normal')
    search_packages_btn.configure(state='normal')
    search_packages_input.configure(state='normal')

### -----------------------------------------------------------------------------
### Create Windows
### -----------------------------------------------------------------------------

### Create Main tkinter Window
window = Tk()
window.title("Tuxheim")

### Contextmenu
make_contextmenu(window)
window.bind_class("Entry", "<Button-3><ButtonRelease-3>", show_contextmenu)

window_width = 950
window_height = 550

# get the screen dimension
screen_width = window.winfo_screenwidth()
screen_height = window.winfo_screenheight()

# find the center point
center_x = int(screen_width/2 - window_width / 2)
center_y = int(screen_height/2 - window_height / 2)

# set the position of the window to the center of the screen
window.geometry(f'{window_width}x{window_height}+{center_x}+{center_y}')

window.iconphoto(False, tkinter.PhotoImage(file=resource_path('icon.png')))

### Change Theme
style = ttk.Style(window)
style.configure('TNotebook', tabposition='wn', background="white")
style.configure('TNotebook.Tab', padding=(10, 7), width=18)

### Create tabs
TAB_CONTROL = ttk.Notebook(window)

### Tab 1 "Connection"
CONNECTION_TAB = ttk.Frame(TAB_CONTROL)
TAB_CONTROL.add(CONNECTION_TAB, text='Connection')

### Tab 2 "system Information"
SYS_TAB = ttk.Frame(TAB_CONTROL)
TAB_CONTROL.add(SYS_TAB, text='System Information', state='disabled')
TAB_CONTROL.pack(expand=1, fill="both")

### Tab 3 "admin permission"
SUDO_TAB = ttk.Frame(TAB_CONTROL)
TAB_CONTROL.add(SUDO_TAB, text='Administrator\nManagement', state='disabled')
TAB_CONTROL.pack(expand=1, fill="both")

### Tab 4 "Group Assignment"
GROUP_TAB = ttk.Frame(TAB_CONTROL)
TAB_CONTROL.add(GROUP_TAB, text='Group Assignment', state='disabled')
TAB_CONTROL.pack(expand=1, fill="both")

### Tab 5 "User & Group Creation"
USER_GROUP_TAB = ttk.Frame(TAB_CONTROL)
TAB_CONTROL.add(USER_GROUP_TAB, text='User & Group\nCreation', state='disabled')
TAB_CONTROL.pack(expand=1, fill="both")

### Tab 6 "File & Directory Permissions"
DIR_TAB = ttk.Frame(TAB_CONTROL)
TAB_CONTROL.add(DIR_TAB, text='File & Directory\nPermissions', state='disabled')
TAB_CONTROL.pack(expand=1, fill="both")

### Tab 7 "Software Management"
SOFTWARE_TAB = ttk.Frame(TAB_CONTROL)
TAB_CONTROL.add(SOFTWARE_TAB, text='Software\nManagement', state='disabled')
TAB_CONTROL.pack(expand=1, fill="both")

### Tab 8 "View Logs"
LOGS_TAB = ttk.Frame(TAB_CONTROL)
TAB_CONTROL.add(LOGS_TAB, text='View Logs', state='disabled')
TAB_CONTROL.pack(expand=1, fill="both")

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.load_system_host_keys()

### Create menu
menubar = Menu(window)
filemenu = Menu(menubar, tearoff=0)
filemenu.add_command(label="About", command=about_popup)
filemenu.add_separator()
filemenu.add_command(label="Exit", command=window.destroy)
menubar.add_cascade(label="File", menu=filemenu)

### -----------------------------------------------------------------------------

### Label Frame "Connection"
connection_frame = LabelFrame(CONNECTION_TAB, text='Connection', width=500, height=180, fg="white")
connection_frame.grid(column=0, row=0, sticky=NSEW)
connection_frame.grid_columnconfigure(0, weight=1)
connection_frame.grid_columnconfigure(1, weight=1)
connection_frame.grid_columnconfigure(2, weight=1)
CONNECTION_TAB.grid_rowconfigure(0, weight=1)
CONNECTION_TAB.grid_columnconfigure(0, weight=1)
connection_frame.config(bg=BG_COLOR)

### Insert image
img_disconnect = Image.open(resource_path("icon_red.png"))
img_disconnect = ImageTk.PhotoImage(img_disconnect)
background_label_disconnect = Label(connection_frame, image=img_disconnect, borderwidth=0, bg=BG_COLOR)
background_label_disconnect.grid(row = 11, column = 1)

img_connect = Image.open(resource_path("icon_green.png"))
img_connect = ImageTk.PhotoImage(img_connect)
background_label_connect = Label(connection_frame, image=img_connect, borderwidth=0, bg=BG_COLOR)
background_label_connect.grid(row = 11, column = 1)
background_label_connect.grid_remove()
state = "Showing"

def hideBG():
    global state
    if state == "Hidden":
        background_label_disconnect.grid(row=11, column=1)
        background_label_connect.grid_remove()
        state = "Showing"

    elif state == "Showing":
        background_label_connect.grid(row=11, column=1)
        background_label_disconnect.grid_remove()
        state = "Hidden"

### Current machine
### Separator
separator = ttk.Separator(connection_frame, orient='horizontal')
separator.grid(row=5, columnspan=5, sticky=EW)

### Blankspace
#lbl_your_machine = Label(connection_frame, text="", fg="white", font=("Arial", 10), bg=BG_COLOR)
#lbl_your_machine.grid(row = 5, column = 0, pady=40)

### Image
lbl_your_machine = Label(connection_frame, text="Your machine", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_your_machine.grid(row = 6, column = 0)

### Hostname
tuxheim_pc_hostname = socket.gethostname()
lbl_hostname = Label(connection_frame, text="Hostname", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_hostname.grid(row = 7, column = 0)

lbl_pc_hostname = Label(connection_frame, text=tuxheim_pc_hostname, fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_pc_hostname.grid(row = 7, column = 1)

### Operating system
used_os = platform.system()
os_version = platform.release()
lbl_os = Label(connection_frame, text="OS", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_os.grid(row = 8, column = 0)

lbl_used_os = Label(connection_frame, text=used_os + " " + os_version, fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_used_os.grid(row = 8, column = 1)

### IP address
ip_address = socket.gethostbyname(socket.gethostname())
lbl_ip_address = Label(connection_frame, text="IP address", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_ip_address.grid(row = 9, column = 0)

lbl_ip = Label(connection_frame, text=ip_address, fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_ip.grid(row = 9, column = 1)

### Connection status
lbl_status = Label(connection_frame, text="Status", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_status.grid(row = 10, column = 0)

### Connection status Text
status_text = Label(connection_frame, text="Not connected", fg="red", font=("Arial", 10, 'bold'), bg=BG_COLOR)
status_text.grid(row = 10, column = 1)

### Hostname Input
lbl_host_ip = Label(connection_frame, text="Host / IP", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_host_ip.grid(row = 0, column = 0)

hostname = Entry(connection_frame, bg="white", fg="black", width=50, bd=5, borderwidth=0, highlightthickness=2, highlightbackground="black")
hostname.bind('<Return>', connection_enter)
hostname.grid(row = 0, column = 1)

### Username Input
lbl_username = Label(connection_frame, text="Username", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_username.grid(row = 1, column = 0)

username = Entry(connection_frame, bg="white", fg="black", width=50, bd=5, borderwidth=0, highlightthickness=2, highlightbackground="black")
username.bind('<Return>', connection_enter)
username.grid(row = 1, column = 1)

### Password Input
lbl_password = Label(connection_frame, text="Password", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_password.grid(row = 2, column = 0)

password = Entry(connection_frame, bg="white", show="*", fg="black", width=50, bd=5, borderwidth=0, highlightthickness=2, highlightbackground="black")
password.bind('<Return>', connection_enter)
password.grid(row = 2, column = 1)

### Port Input
lbl_ssh = Label(connection_frame, text="SSH-Port", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_ssh.grid(row = 3, column = 0)

port = Entry(connection_frame, bg="white", fg="black", width=50, bd=5, borderwidth=0, highlightthickness=2, highlightbackground="black")
port.bind('<Return>', connection_enter)
port.insert(0, "22")
port.grid(row = 3, column = 1)

### Connection-Button
connect_btn = Button(connection_frame, text="Connect", fg="green", width=20, command=lambda: [connect_est()], bg='white')
connect_btn.grid(row = 0, column = 2)

### Disconnect-Button
disconnect_btn = Button(connection_frame, text="Disconnect", fg="red", width=20, bg='white', command=lambda: [connect_close(), connect_enable(), clear_users(), clear_groups(), clear_add_rm_user(), check_connection(), tabs_disable(), connect_clear()])
disconnect_btn.configure(state='disabled')
disconnect_btn.grid(row = 1, column = 2)

### "Open in Terminal" Button
terminal_btn = Button(connection_frame, text="Open in Terminal", bg='white', width=20, fg="black", command=lambda: [open_terminal_function()])
terminal_btn.configure(state='disabled')
terminal_btn.grid(row = 2, column = 2)

### Checkbox to keep login information
keep_login = IntVar(value=1)
keep_login_checkbox = Checkbutton(connection_frame, text = "Keep login information", variable = keep_login, onvalue = 1, offvalue = 0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white',selectcolor="black")
keep_login_checkbox.grid(row = 4, column = 0)

### -----------------------------------------------------------------------------

### Label Frame "System Information"
system_frame = LabelFrame(SYS_TAB, text='System Information', width=500, height=180, fg="white")
system_frame.grid(column=0, row=0, sticky=NSEW)
system_frame.grid_columnconfigure(0, weight=1)
system_frame.grid_columnconfigure(1, weight=1)
system_frame.grid_columnconfigure(2, weight=1)
SYS_TAB.grid_rowconfigure(0, weight=1)
SYS_TAB.grid_columnconfigure(0, weight=1)
system_frame.config(bg=BG_COLOR)

### Hostname
lbl_hostname = Label(system_frame, text="Hostname", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_hostname.grid(row = 0, column = 0)

hostname_lbl = Label(system_frame, text="UNKOWN", fg="white", font=("Arial", 10), bg=BG_COLOR)
hostname_lbl.grid(row = 0, column = 1)

### OS-release
lbl_os_release = Label(system_frame, text="OS-release", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_os_release.grid(row = 1, column = 0)
os_lbl = Label(system_frame, text="UNKOWN", fg="white", font=("Arial", 10), bg=BG_COLOR)
os_lbl.grid(row = 1, column = 1)

### Machine model
lbl_machine_model = Label(system_frame, text="Machine model", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_machine_model.grid(row = 2, column = 0)
model_lbl = Label(system_frame, text="UNKOWN", fg="white", font=("Arial", 10), bg=BG_COLOR)
model_lbl.grid(row = 2, column = 1)

### Reboot required
lbl_reboot_required = Label(system_frame, text="Reboot required", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_reboot_required.grid(row = 3, column = 0)
restart_lbl = Label(system_frame, text="UNKOWN", fg="white", font=("Arial", 10), bg=BG_COLOR)
restart_lbl.grid(row = 3, column = 1)

### Button "Shutdown"
shutdown_btn = Button(system_frame, text="Shutdown", width=20, command=lambda: [shutdown()], bg='white')
shutdown_btn.grid(row = 0, column = 2)

### Button "Reboot"
reboot_btn = Button(system_frame, text="Reboot", width=20, command=lambda: [reboot()], bg='white')
reboot_btn.grid(row = 1, column = 2)

### Label "Automatic updates"
lbl_automatic_updates = Label(system_frame, text="Automatic updates", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_automatic_updates.grid(row = 4, column = 0)
automatic_updates_lbl = Label(system_frame, text="UNKOWN", fg="white", font=("Arial", 10), bg=BG_COLOR)
automatic_updates_lbl.grid(row = 4, column = 1)

### Label "Firewall status"
lbl_firewall_status = Label(system_frame, text="Firewall status", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_firewall_status.grid(row = 5, column = 0)
firewall_status_lbl = Label(system_frame, text="UNKOWN", fg="white", font=("Arial", 10), bg=BG_COLOR)
firewall_status_lbl.grid(row = 5, column = 1)

### Button "Activate automatic updates"
automatic_updates_btn = Button(system_frame, text="Activate automatic updates", width=20, command=lambda: [activate_automatic_updates(),get_automatic_updates_status()], bg='white')
automatic_updates_btn.grid(row = 4, column = 2)

### Button "Activate Firewall"
activate_firewall_btn = Button(system_frame, text="Activate Firewall", width=20, command=lambda: [activate_firewall(),get_firewall_status()], bg='white')
activate_firewall_btn.grid(row = 5, column = 2)

### Insert image
img_sys = Image.open(resource_path("tux_sys.png"))
img_sys = img_sys.resize((200, 190), Image.LANCZOS)
img_sys = ImageTk.PhotoImage(img_sys)
panel = Label(system_frame, image=img_sys, borderwidth=0, bg=BG_COLOR)
panel.grid(row = 6, column = 1, pady=30)

### -----------------------------------------------------------------------------

### Label Frame "Administrator Management"
sudo_frame = LabelFrame(SUDO_TAB, text='Administrator Management', width=500, height=180, fg='white')
sudo_frame.grid(column=0, row=0, sticky=NSEW)
sudo_frame.grid_columnconfigure(0, weight=1)
sudo_frame.grid_columnconfigure(1, weight=1)
sudo_frame.grid_columnconfigure(2, weight=1)
sudo_frame.grid_columnconfigure(3, weight=1)
SUDO_TAB.grid_rowconfigure(0, weight=1)
SUDO_TAB.grid_columnconfigure(0, weight=1)
sudo_frame.config(bg=BG_COLOR)

### Headline "User with admin permissions"
lbl_current_admin_users = Label(sudo_frame, text="Current admin users", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_current_admin_users.grid(row = 0, column = 2)

### Add a subframe with listbox and scrollbar
sudo_subframe = tkinter.Frame(sudo_frame, background='white')
sudo_subframe.grid(row = 1, column = 2, rowspan=3)

lbox_sudo = Listbox(sudo_subframe, height=5)
lbox_sudo.grid(ipadx=20, columnspan=2, row=0, column=0)
lbox_sudo.bind('<<ListboxSelect>>', sudo_expiration_selected)

vsb = ttk.Scrollbar(sudo_subframe, orient="vertical", command=lbox_sudo.yview)
vsb.grid(row = 0, column = 1, sticky=E, ipady=17)
lbox_sudo.configure(yscrollcommand=vsb.set)

### Headline "Permissions will expire in (days)"
lbl_permissions_expire = Label(sudo_frame, text="Permissions will\nexpire in (days)", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_permissions_expire.grid(row = 0, column = 3)

### Listbox for days of permissions left
sudo_subframe_expiration = tkinter.Frame(sudo_frame, background='white')
sudo_subframe_expiration.grid(row = 1, column = 3)
lbox_expiration = Listbox(sudo_subframe_expiration, height=1)
lbox_expiration.pack()

### Button "Refresh list"
cur_admin_btn = Button(sudo_frame, text="Refresh list", fg="black", width=20, bg='white', command=lambda: [clear_listbox_expiration(), list_admin()])
cur_admin_btn.grid(row = 5, column = 2, ipadx=15)

### Input user
input_sudo = Entry(sudo_frame, bg="white", fg="black", bd=5, width=30, borderwidth=0, highlightthickness=2, highlightbackground="black")
input_sudo.bind('<Return>', admin_enter)
input_sudo.grid(row = 1, column = 1)

### Label "Username"
lbl_admin_username = Label(sudo_frame, text="Username", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_admin_username.grid(row = 1, column = 0)

### Label "Expiration"
lbl_expire_in_days = Label(sudo_frame, text="Expire in (days)", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_expire_in_days.grid(row = 2, column = 0)

### Input expiration
input_expiration = Entry(sudo_frame, bg="white", fg="black", bd=5, width=30, borderwidth=0, highlightthickness=2, highlightbackground="black")
input_expiration.bind('<Return>', admin_enter)
input_expiration.grid(row = 2, column = 1)

### Button "Add user as admin"
add_admin_user_btn = Button(sudo_frame, text="Add user as admin", fg="black", width=20, bg='white', command=lambda: [sudo_add_function(), clear_listbox_expiration(), list_admin()])
add_admin_user_btn.grid(row = 5, column = 1, ipadx=15)

### Button "Remove user from admin"
rm_admin_user_btn = Button(sudo_frame, text="Remove selected user as admin", fg="black", width=20, bg='white', command=lambda: [sudo_rm_function(), clear_listbox_expiration(), list_admin()])
rm_admin_user_btn.grid(row = 6, column = 2, ipadx=15)

### Checkbox to grant unlimited sudo access
unlimited_sudo = IntVar(value=0)
unlimited_sudo_checkbox = Checkbutton(sudo_frame, text="Unlimited days", variable=unlimited_sudo, onvalue=1, offvalue=0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white', selectcolor="black", command=click_sudo_checkbutton)
unlimited_sudo_checkbox.grid(row = 4, column = 1)

### Insert image
img_admin = Image.open(resource_path("tux_admin.png"))
img_admin = img_admin.resize((250, 170), Image.LANCZOS)
img_admin = ImageTk.PhotoImage(img_admin)
panel = Label(sudo_frame, image=img_admin, borderwidth=0, bg=BG_COLOR)
panel.grid(row = 7, column = 1, pady=30, columnspan=2)

### -----------------------------------------------------------------------------

### Label Frame "Group Assignment"
group_frame = LabelFrame(GROUP_TAB, text='Group Assignment', width=500, height=180, fg='white')
group_frame.grid(column=0, row=0, sticky=NSEW)
group_frame.grid_columnconfigure(1, weight=1)
GROUP_TAB.grid_rowconfigure(0, weight=1)
GROUP_TAB.grid_columnconfigure(0, weight=1)
group_frame.grid_columnconfigure(0, weight=1)
group_frame.config(bg=BG_COLOR)

### Headline "Groups"
lbl_groups = Label(group_frame, text="Groups", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_groups.grid(row = 0, column = 0)

### Add a subframe with listbox and scrollbar
group_subframe1 = tkinter.Frame(group_frame, background='white')
group_subframe1.grid(row = 1, column = 0)

lbox_groups = Listbox(group_subframe1, exportselection=False)
lbox_groups.bind('<<ListboxSelect>>', items_selected)
lbox_groups.grid(ipadx=80, row=0, column=0)

vsb1 = ttk.Scrollbar(group_subframe1, orient="vertical", command=lbox_groups.yview)
vsb1.grid(row = 0, column = 1, sticky=E, ipady=55)
lbox_groups.configure(yscrollcommand=vsb1.set)

### Headline "Members"
lbl_members = Label(group_frame, text="Members", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_members.grid(row = 0, column = 1)

### list users
group_subframe2 = tkinter.Frame(group_frame, background='white')
group_subframe2.grid(row = 1, column = 1)

lbox_users = Listbox(group_subframe2)
lbox_users.grid(ipadx=80, row=0, column=0)
vsb2 = ttk.Scrollbar(group_subframe2, orient="vertical", command=lbox_users.yview)
vsb2.grid(row = 0, column = 1, sticky=E, ipady=55)
lbox_users.configure(yscrollcommand=vsb2.set)

### Add given user to selected group
group_subframe3 = tkinter.Frame(group_frame, background='white')
group_subframe3.grid(row = 3, column = 0)

add_rm_user = Entry(group_subframe3, bg="white", width=39, fg="black", bd=5, borderwidth=0, highlightthickness=2, highlightbackground="black")
add_rm_user.bind('<Return>', group_enter)
add_rm_user.grid(row = 0, column = 1)

### Add / remove user button
### Headline "Username"
lbl_username_group = Label(group_subframe3, text="Username", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_username_group.grid(row = 0, column = 0)
add_group_user_btn = Button(group_frame, text="Add user to selected group", width=42, fg="black", command=lambda: [add_user_function()], bg='white')
add_group_user_btn.grid(row = 2, column = 0)
rm_group_user_btn = Button(group_frame, text="Remove selected user from group", width=42, fg="black", command=lambda: [rm_user_funtion()], bg='white')
rm_group_user_btn.grid(row = 2, column = 1)

### "Rescan groups" button
rescan_group_btn = Button(group_frame, text="Rescan groups", fg="black", width=42, command=lambda: [clear_groups(), clear_users(), list_groups()], bg='white')
rescan_group_btn.grid(row = 4, column = 0)

### Insert image
img_group1 = Image.open(resource_path("tux_group1.png"))
img_group1 = img_group1.resize((250, 185), Image.LANCZOS)
img_group1 = ImageTk.PhotoImage(img_group1)
panel = Label(group_frame, image=img_group1, borderwidth=0, bg=BG_COLOR)
panel.grid(row = 5, column = 1, pady=30)

### -----------------------------------------------------------------------------

### Label Frame "User & Group Creation"
users_group_frame = LabelFrame(USER_GROUP_TAB, text='User & Group Creation', width=500, height=180, fg='white')
users_group_frame.grid(column=0, row=0, sticky=NSEW)
users_group_frame.grid_columnconfigure(0, weight=1)
users_group_frame.grid_columnconfigure(1, weight=1)
USER_GROUP_TAB.grid_rowconfigure(0, weight=1)
USER_GROUP_TAB.grid_columnconfigure(0, weight=1)
users_group_frame.config(bg=BG_COLOR)

### Headline "Users"
lbl_users_user_group = Label(users_group_frame, text="Users", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_users_user_group.grid(row = 0, column = 0)

user_group_subframe1 = tkinter.Frame(users_group_frame, background='white')
user_group_subframe1.grid(row = 1, column = 0)

columns_user_group_user = ['Name', 'UID', 'GID']
tree_user_group_user = ttk.Treeview(user_group_subframe1, columns=columns_user_group_user, show='headings', height=10)

### listbox for directories
tree_user_group_user.column(columns_user_group_user[0], anchor=CENTER, width=80)
tree_user_group_user.heading(columns_user_group_user[0], text=columns_user_group_user[0], command=lambda: treeview_sort_column(columns_user_group_user[0], False, tree_user_group_user))

tree_user_group_user.column(columns_user_group_user[1], anchor=CENTER, width=80)
tree_user_group_user.heading(columns_user_group_user[1], text=columns_user_group_user[1], command=lambda: treeview_sort_column(columns_user_group_user[1], False, tree_user_group_user))

tree_user_group_user.column(columns_user_group_user[2], anchor=CENTER, width=80)
tree_user_group_user.heading(columns_user_group_user[2], text=columns_user_group_user[2], command=lambda: treeview_sort_column(columns_user_group_user[2], False, tree_user_group_user))

tree_user_group_user.grid(ipadx=15)

vsb = ttk.Scrollbar(user_group_subframe1, orient="vertical", command=tree_user_group_user.yview)
vsb.grid(row = 0, column = 1, sticky=E, ipady=87)
tree_user_group_user.configure(yscrollcommand=vsb.set)

### Headline "Groups"
lbl_groups_user_group = Label(users_group_frame, text="Groups", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_groups_user_group.grid(row = 0, column = 1)

### Add a subframe for groups with listbox and scrollbar
user_group_subframe2 = tkinter.Frame(users_group_frame, background='white')
user_group_subframe2.grid(row = 1, column = 1)

columns_user_group_group = ['Name', 'GID']
tree_user_group_group = ttk.Treeview(user_group_subframe2, columns=columns_user_group_group, show='headings', height=10)

### listbox for directories
tree_user_group_group.column(columns_user_group_group[0], anchor=CENTER, width=125)
tree_user_group_group.heading(columns_user_group_group[0], text=columns_user_group_group[0], command=lambda: treeview_sort_column(columns_user_group_group[0], False, tree_user_group_group))

tree_user_group_group.column(columns_user_group_group[1], anchor=CENTER, width=125)
tree_user_group_group.heading(columns_user_group_group[1], text=columns_user_group_group[1], command=lambda: treeview_sort_column(columns_user_group_group[1], False, tree_user_group_group))

tree_user_group_group.grid(ipadx=15)

vsb = ttk.Scrollbar(user_group_subframe2, orient="vertical", command=tree_user_group_group.yview)
vsb.grid(row = 0, column = 1, sticky=E, ipady=87)
tree_user_group_group.configure(yscrollcommand=vsb.set)

### Remove "selected user" button
delete_user = Button(users_group_frame, text="Delete selected user", fg="black", command=lambda: [delete_user_function(),list_users_tree_function(),list_groups_tree_function()], bg='white')
delete_user.grid(row = 2, column = 0, ipadx=88)
### Checkbox to delete home directory
del_home = IntVar(value=1)
del_home_checkbox = Checkbutton(users_group_frame, text="Delete /home directory", variable=del_home, onvalue=1, offvalue=0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white',selectcolor="black")
del_home_checkbox.grid(row = 3, column = 0)

### "Delete selected group" button
delete_group = Button(users_group_frame, text="Delete selected group", fg="black", command=lambda: [delete_group_function(),list_users_tree_function(),list_groups_tree_function()], bg='white')
delete_group.grid(row = 2, column = 1, ipadx=88)

### Rescan users and groups button
rescan_users_groups = Button(users_group_frame, text="Rescan users and groups", fg="black", command=lambda: [list_users_tree_function(),list_groups_tree_function()], bg='white')
rescan_users_groups.grid(row = 4, column = 0, columnspan=2, ipadx=220)

### Separator
separator = ttk.Separator(users_group_frame, orient='horizontal')
separator.grid(row=5, columnspan=5, sticky=EW)

### "Username"-Label
username_lbl = Label(users_group_frame, text="Username", fg="white", font=("Arial", 10), bg=BG_COLOR)
username_lbl.grid(row = 6, column = 0)

### Inputfield "Username"
create_user_input = Entry(users_group_frame, bg="white", fg="black", bd=5, borderwidth=0, highlightthickness=2, highlightbackground="black")
create_user_input.bind('<Return>', create_user_enter)
create_user_input.grid(row = 7, column = 0, ipadx=70)

### Checkbutton "Create /home-directory"
create_home = IntVar(value=1)
create_home_btn = Checkbutton(users_group_frame, text="Create /home", variable=create_home, onvalue=1, offvalue=0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white', selectcolor="black")
create_home_btn.grid(row = 10, column = 0)

### Checkbutton "Set UID"
user_group_subframe3 = tkinter.Frame(users_group_frame, background=BG_COLOR)
user_group_subframe3.grid(row = 8, column = 0)

set_uid = IntVar(value=0)
set_uid_btn = Checkbutton(user_group_subframe3, text="Set UID", variable=set_uid, onvalue=1, offvalue=0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white', selectcolor="black", command=click_uid_checkbutton)
set_uid_btn.grid(row = 0, column = 0)
### Inputfield "UID"
input_uid = Entry(user_group_subframe3, bg="white", fg="black", bd=5, borderwidth=0, highlightthickness=2, highlightbackground="black", state=DISABLED)
input_uid.bind('<Return>', create_user_enter)
input_uid.grid(row = 0, column = 1, ipadx=37)

### Checkbutton "Set GID"
user_group_subframe4 = tkinter.Frame(users_group_frame, background=BG_COLOR)
user_group_subframe4.grid(row = 8, column = 1)

set_gid = IntVar(value=0)
set_gid_btn = Checkbutton(user_group_subframe4, text="Set GID", variable=set_gid, onvalue=1, offvalue=0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white', selectcolor="black", command=click_gid_checkbutton)
set_gid_btn.grid(row = 0, column = 0)

### Inputfield "GID"
input_gid = Entry(user_group_subframe4, bg="white", fg="black", bd=5, borderwidth=0, highlightthickness=2, highlightbackground="black", state=DISABLED)
input_gid.bind('<Return>', create_group_enter)
input_gid.grid(row = 0, column = 1, ipadx=37)

### "Create user" Button
create_user_btn = Button(users_group_frame, text="Create user", bg='white', fg="black", command=lambda: [create_user_function(),list_users_tree_function(),list_groups_tree_function()])
create_user_btn.grid(row = 10, column = 0, ipadx=95)

### "Group"-Label
group_lbl = Label(users_group_frame, text="Group", fg="white", font=("Arial", 10), bg=BG_COLOR)
group_lbl.grid(row = 6, column = 1)

### Inputfield "Group"
create_group_input = Entry(users_group_frame, bg="white", fg="black", bd=5, borderwidth=0, highlightthickness=2, highlightbackground="black")
create_group_input.bind('<Return>', create_group_enter)
create_group_input.grid(row = 7, column = 1, ipadx=70)

### "Create group" Button
create_group_btn = Button(users_group_frame, text="Create group", bg='white', fg="black", command=lambda: [create_group_function(),list_users_tree_function(),list_groups_tree_function()])
create_group_btn.grid(row = 10, column = 1, ipadx=95)

### -----------------------------------------------------------------------------

### Label Frame "File & Directory Permissions"
dir_frame = LabelFrame(DIR_TAB, text='File & Directory Permissions', width=500, height=180, fg='white')
dir_frame.grid(column=0, row=0, sticky=NSEW)
dir_frame.grid_columnconfigure(0, weight=1)
DIR_TAB.grid_rowconfigure(0, weight=1)
DIR_TAB.grid_columnconfigure(0, weight=1)
dir_frame.config(bg=BG_COLOR)

recurse_owner = IntVar()
recurse_permissions = IntVar()
read_permissions = IntVar()
write_permissions = IntVar()
execute_permissions = IntVar()
user_permissions = IntVar()
group_permissions = IntVar()
other_permissions = IntVar()

### Add a subframe with treeview and scrollbar
dir_subframe = tkinter.Frame(dir_frame, background='white')
dir_subframe.grid(row = 0, column = 0)

columns_file_directory = ['Permission', 'UID', 'GID', 'File & Directory']
tree_file_directory = ttk.Treeview(dir_subframe, columns=columns_file_directory, show='headings', height=5)

### listbox for directories
tree_file_directory.column(columns_file_directory[0], anchor=CENTER, width=150)
tree_file_directory.heading(columns_file_directory[0], text=columns_file_directory[0], command=lambda: treeview_sort_column(columns_file_directory[0], False, tree_file_directory))

### listbox for directories
tree_file_directory.column(columns_file_directory[1], anchor=CENTER, width=108)
tree_file_directory.heading(columns_file_directory[1], text=columns_file_directory[1], command=lambda: treeview_sort_column(columns_file_directory[1], False, tree_file_directory))

### listbox for directories
tree_file_directory.column(columns_file_directory[2], anchor=CENTER, width=108)
tree_file_directory.heading(columns_file_directory[2], text=columns_file_directory[2], command=lambda: treeview_sort_column(columns_file_directory[2], False, tree_file_directory))

### listbox for directories
tree_file_directory.column(columns_file_directory[3], anchor=CENTER, width=150)
tree_file_directory.heading(columns_file_directory[3], text=columns_file_directory[3], command=lambda: treeview_sort_column(columns_file_directory[3], False, tree_file_directory))

tree_file_directory.grid(row=0, column=0, ipadx=55, ipady=50)

vsb = ttk.Scrollbar(dir_subframe, orient="vertical", command=tree_file_directory.yview)
vsb.grid(row = 0, column = 1, sticky=E, ipady=87)
tree_file_directory.configure(yscrollcommand=vsb.set)

### Headline "Path"
dir_subframe1 = tkinter.Frame(dir_frame, background=BG_COLOR)
dir_subframe1.grid(row = 1, column = 0)

lbl_path = Label(dir_subframe1, text="Path", bg=BG_COLOR, fg="white", font=("Arial", 10))
lbl_path.grid(row = 0, column = 0)
path_input = Entry(dir_subframe1, bg="white", fg="black", bd=5, borderwidth=0, highlightthickness=2, highlightbackground="black")
path_input.insert(0, "/")
path_input.bind('<Return>', list_path_enter)
path_input.grid(row = 0, column = 1, ipadx=130)

### List Path-Button
list_path_btn = Button(dir_subframe1, text="List path", bg='white', fg="black", command=lambda: [fill_table_function()])
list_path_btn.grid(row = 0, column = 2)

### Checkbuttons
dir_subframe2 = tkinter.Frame(dir_frame, background=BG_COLOR)
dir_subframe2.grid(row = 2, column = 0)

### user
user = Checkbutton(dir_subframe2, text = "user", variable = user_permissions, onvalue = 1, offvalue = 0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white',selectcolor="black")
user.grid(row = 0, column = 0, sticky=W)
### group
group = Checkbutton(dir_subframe2, text = "group", variable = group_permissions, onvalue = 1, offvalue = 0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white',selectcolor="black")
group.grid(row = 0, column = 1, sticky=W)
### other
other = Checkbutton(dir_subframe2, text = "other", variable = other_permissions, onvalue = 1, offvalue = 0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white',selectcolor="black")
other.grid(row = 0, column = 2, sticky=W)
### read
read = Checkbutton(dir_subframe2, text = "read", variable = read_permissions, onvalue = 1, offvalue = 0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white',selectcolor="black")
read.grid(row = 1, column = 0, sticky=W)
### write
write = Checkbutton(dir_subframe2, text = "write", variable = write_permissions, onvalue = 1, offvalue = 0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white',selectcolor="black")
write.grid(row = 1, column = 1, sticky=W)
### execute
execute = Checkbutton(dir_subframe2, text = "execute", variable = execute_permissions, onvalue = 1, offvalue = 0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white',selectcolor="black")
execute.grid(row = 1, column = 2, sticky=W)
### recursive
recurse_permission = Checkbutton(dir_subframe2, text = "recursive", variable = recurse_permissions, onvalue = 1, offvalue = 0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white',selectcolor="black")
recurse_permission.grid(row = 2, column = 0, sticky=W)
### Set permissions
set_permissions_btn = Button(dir_subframe2, text="Add permission", width=20, bg='white', fg="black", command= lambda: [thread_add_permissions_function(), fill_table_function()])
set_permissions_btn.grid(row = 3, column = 0, sticky=W)

### del permissions
del_permissions_btn = Button(dir_subframe2, text="Remove permission", width=20, bg='white', fg="black", command= lambda: [thread_del_permissions_function(), fill_table_function()])
del_permissions_btn.grid(row = 3, column = 1)

### Separator
separator = ttk.Separator(dir_frame, orient='horizontal')
separator.grid(row=3, columnspan=5, sticky=EW)

### Set ownership
dir_subframe3 = tkinter.Frame(dir_frame, background=BG_COLOR)
dir_subframe3.grid(row = 4, column = 0)

### UID
lbl_user_ownership = Label(dir_subframe3, text="User", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_user_ownership.grid(row = 0, column = 0)
ownership_user_input = Entry(dir_subframe3, bg="white", fg="black", bd=5, borderwidth=0, highlightthickness=2, highlightbackground="black")
ownership_user_input.bind('<Return>', set_ownership_enter)
ownership_user_input.grid(row = 0, column = 1, ipadx=30)

### GID
lbl_group_ownership = Label(dir_subframe3, text="Group", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_group_ownership.grid(row = 1, column = 0)
ownership_group_input = Entry(dir_subframe3, bg="white", fg="black", bd=5, borderwidth=0, highlightthickness=2, highlightbackground="black")
ownership_group_input.bind('<Return>', set_ownership_enter)
ownership_group_input.grid(row = 1, column = 1, ipadx=30)

### Checkbutton recursive
recurse = Checkbutton(dir_frame, text="recursive", variable=recurse_owner, onvalue=1, offvalue=0, bg=BG_COLOR, fg='white', activebackground=BG_COLOR, activeforeground='white', selectcolor="black")
recurse.grid(row = 5, column = 0)

### Set ownership Button
set_ownership_btn = Button(dir_frame, text="Set ownership", bg='white', fg="black", command=lambda: [thread_set_ownership_function(), fill_table_function()])
set_ownership_btn.grid(row = 6, column = 0)

### Insert image
img_files = Image.open(resource_path("tux_files.png"))
img_files = img_files.resize((120, 120), Image.LANCZOS)
img_files = ImageTk.PhotoImage(img_files)
panel = Label(dir_frame, image=img_files, borderwidth=0, bg=BG_COLOR)
#panel.grid(row = 7, column = 0, sticky=E)

### -----------------------------------------------------------------------------

### Label Frame "Software Management"
software_frame = LabelFrame(SOFTWARE_TAB, text='Software Management', width=500, height=180, fg='white')
software_frame.grid(column=0, row=0, sticky=NSEW)
software_frame.grid_columnconfigure(0, weight=1)
SOFTWARE_TAB.grid_rowconfigure(0, weight=1)
SOFTWARE_TAB.grid_columnconfigure(0, weight=1)
software_frame.config(bg=BG_COLOR)

### Add a subframe with treeview and scrollbar
software_subframe = tkinter.Frame(software_frame, background='white')
software_subframe.grid(row = 0, column = 0)

columns_software = ['Package', 'Version', 'Status']
tree_software = ttk.Treeview(software_subframe, columns=columns_software, show='headings', height=5)

### listbox for directories
tree_software.column(columns_software[0], anchor=CENTER, width=171)
tree_software.heading(columns_software[0], text=columns_software[0], command=lambda: treeview_sort_column(columns_software[0], False, tree_software))

tree_software.column(columns_software[1], anchor=CENTER, width=171)
tree_software.heading(columns_software[1], text=columns_software[1], command=lambda: treeview_sort_column(columns_software[1], False, tree_software))

tree_software.column(columns_software[2], anchor=CENTER, width=171)
tree_software.heading(columns_software[2], text=columns_software[2], command=lambda: treeview_sort_column(columns_software[2], False, tree_software))

tree_software.grid(row = 0, column = 0, ipadx=65, ipady=50)

vsb = ttk.Scrollbar(software_subframe, orient="vertical", command=tree_software.yview)
vsb.grid(row = 0, column = 0, sticky=E, ipady=87)
tree_software.configure(yscrollcommand=vsb.set)

### Label "Package name"
software_subframe2 = tkinter.Frame(software_frame, bg=BG_COLOR)
software_subframe2.grid(row = 2, column = 0)

lbl_package_name = Label(software_subframe2, text="Package name", fg="white", font=("Arial", 10), bg=BG_COLOR)
lbl_package_name.grid(row = 0, column = 0)

### Entry field "Search packages"
search_packages_input = Entry(software_subframe2, bg="white", fg="black", bd=5, borderwidth=0, highlightthickness=2, highlightbackground="black")
search_packages_input.bind('<Return>', search_packages_function_enter)
search_packages_input.grid(row = 0, column = 1, ipadx=120)

### Search packages-Button
search_packages_btn = Button(software_subframe2, text="🔎", fg="black", bg='white', command=lambda: [search_packages_function()])
search_packages_btn.grid(row = 0, column = 2)

### Install package-Button
software_subframe1 = tkinter.Frame(software_frame, background='white')
software_subframe1.grid(row = 1, column = 0)

install_packages_btn = Button(software_subframe1, text="Install or update selected package", width=35, bg='white', fg="black", command=lambda: [thread_install_package_function()])
install_packages_btn.grid(row = 0, column = 0)

### Remove package-Button
remove_packages_btn = Button(software_subframe1, text="Remove selected package", width=35, bg='white', fg="black", command=lambda: [thread_remove_package_function()])
remove_packages_btn.grid(row = 0, column = 1)

### "Update packages"-Button
software_subframe3 = tkinter.Frame(software_frame, bg=BG_COLOR)
software_subframe3.grid(row = 3, column = 0)

update_packages_btn = Button(software_subframe3, text="Update packages", width=35, fg="black", bg='white', command=lambda: [thread_update_packages()])
update_packages_btn.grid(row = 0, column = 0)

### "Recreate package cache"-Button
recreate_packages_cache_btn = Button(software_subframe3, text="Recreate package cache", width=35, fg="black", bg='white', command=lambda: [thread_recreate_package_cache()])
recreate_packages_cache_btn.grid(row = 0, column = 1)

### "Check package updates"-Button
check_packages_update_btn = Button(software_frame, text="List available updates", width=35, fg="black", bg='white', command=lambda: [check_package_updates()])
check_packages_update_btn.grid(row = 4, column = 0)

### Headline "Distro Warning"
distro_warn_software_lbl = Label(software_frame, bg=BG_COLOR)
distro_warn_software_lbl.grid(row = 5, column = 0)

### Insert image
img_software = Image.open(resource_path("tux_software.png"))
img_software = img_software.resize((150, 145), Image.LANCZOS)
img_software = ImageTk.PhotoImage(img_software)
panel = Label(software_frame, image=img_software, borderwidth=0, bg=BG_COLOR)
panel.grid(row = 6, column = 0)

### -----------------------------------------------------------------------------

### Label Frame "View Logs"
logs_frame = LabelFrame(LOGS_TAB, text='View Logs', width=500, height=180, fg='white')
logs_frame.grid(column=0, row=0, sticky=NSEW)
logs_frame.grid_columnconfigure(0, weight=1)
LOGS_TAB.grid_rowconfigure(0, weight=1)
LOGS_TAB.grid_columnconfigure(0, weight=1)
logs_frame.config(bg=BG_COLOR)

### Add a subframe with listbox and scrollbar
logs_subframe = tkinter.Frame(logs_frame, background='white')
logs_subframe.grid(row = 1, column = 0)

lbox_logs = Listbox(logs_subframe)
lbox_logs.grid(row = 0, column = 0, ipadx=250, ipady=31)

vsb = ttk.Scrollbar(logs_subframe, orient="vertical", command=lbox_logs.yview)
vsb.grid(row = 0, column = 1, sticky=E, ipady=88)
lbox_logs.configure(yscrollcommand=vsb.set)

### "Show logs"-Button
show_log_btn = Button(logs_frame, text="Show log", bg='white', fg="black", command= lambda: [show_logs()])
show_log_btn.grid(row = 2, column = 0)

### Insert image
img_logs = Image.open(resource_path("tux_logs.png"))
img_logs = img_logs.resize((250, 150), Image.LANCZOS)
img_logs = ImageTk.PhotoImage(img_logs)
panel = Label(logs_frame, image=img_logs, borderwidth=0, bg=BG_COLOR)
panel.grid(row = 3, column = 0, pady=30)

window.config(menu=menubar)
window.mainloop()
